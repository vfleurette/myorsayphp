<?php

/**
 * SlimTwig - Another Slim view adapter for Twig template engine.
 *
 * @author Benjamin GILLET <bgillet@hotmail.fr>
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace BenGee\Slim\Twig;

use \BenGee\Slim\Utils\StringUtils;

/**
 * Slim view adapter for Twig template engine.
 * @author Benjamin GILLET <bgillet@hotmail.fr>
 */
class TwigView extends \Slim\View
{
    /**
     * Twig template loader.
     * @var \Twig_Loader_Filesystem
     */
    protected $_loader = null;

    /**
     * Twig renderer.
     * @var \Twig_Environment
     */
    protected $_renderer = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_loader = new \Twig_Loader_Filesystem();
        $this->_renderer = new \Twig_Environment($this->_loader);
    }
    
    /**
     * Wrapping method to redirect methods not available in this class to the
     * internal instance of the Twig rendering engine.
     * @param string $name Unknown method to call in the internal Twig rendering engine.
     * @param array $arguments Method's arguments.
     * @return mixed Result of the called method.
     */
    public function __call($name, $arguments)
    {
        call_user_func_array(array($this->_renderer, $name), $arguments);
    }
    
    /**
     * Wrapping method to redirect static methods not available in this class
     * to the internal instance of the Twig rendering engine.
     * @param string $name Unknown static method to call in the internal Twig rendering engine.
     * @param array $arguments Method's arguments.
     * @return mixed Result of the called static method.
     */
    public static function __callStatic($name, $arguments)
    {
        call_user_func_array(array('\\Twig_Environment', $name), $arguments);
    }

    /**
     * Set one or more directory where renderer will look for templates.
     * Be aware that calling this method replaces currently set directories.
     * @param string|array $directory A single directory (string) or an array using namespace as key and path as value.
     */
    public function setTemplatesDirectory($directory)
    {
        if (StringUtils::emptyOrSpaces($directory)) $directory = array();
        if (!is_array($directory)) $directory = array($directory);
        $this->_loader = new \Twig_Loader_Filesystem();
        foreach($directory as $namespace => $path)
        {
            if (StringUtils::emptyOrSpaces($namespace))
            {
                $this->_loader->addPath($path);
            }
            else
            {
                $this->_loader->addPath($path, trim($namespace));
            }
        }
        $this->_renderer->setLoader($this->_loader);
    }

    /**
     * Get currently set templates directories.
     * @return array An array of all referenced templates directories with their namespace as key and path as value.
     */
    public function getTemplatesDirectory()
    {
        return $this->_loader->getPaths();
    }

    /**
     * Add a templates directory to the end of the list of existing ones known by the current Twig templates loader.
     * @param string $path Path to the templates directory to add.
     * @param string $namespace Namespace used to reference the directory inside the Twig templates loader.
     */
    public function addTemplatesDirectory($path, $namespace = false)
    {
        if (StringUtils::emptyOrSpaces($namespace))
        {
            $this->_loader->addPath($path);
        }
        else
        {
            $this->_loader->addPath($path, trim($namespace));
        }
    }

    /**
     * Add a templates directory to the beginning of the list of existing ones known by the current Twig templates loader.
     * @param string $path Path to the templates directory to add.
     * @param string $namespace Namespace used to reference the directory inside the Twig templates loader.
     */
    public function prependTemplatesDirectory($path, $namespace = false)
    {
        if (StringUtils::emptyOrSpaces($namespace))
        {
            $this->_loader->prependPath($path);
        }
        else
        {
            $this->_loader->prependPath($path, trim($namespace));
        }
    }

    /**
     * Get templates directories namespaces.
     * @return array An array of namespaces used to reference templates directories.
     */
    public function getTemplatesNamespaces()
    {
        return $this->_loader->getNamespaces();
    }

    /**
     * Get a fully qualified path to a given template.
     * @param string $file Filename of the template to find in referenced templates directories.
     * @param string $namespace Namespace of a specific set of templates directories.
     * @return string The full pathname to the template or 'false' if not found.
     */
    public function getTemplatePathname($file, $namespace = false)
    {
        $pathname = false;
        if ($this->_loader->exists($file))
        {
            $paths = (StringUtils::emptyOrSpaces($namespace) ? $this->_loader->getPaths() : $this->_loader->getPaths(trim($namespace)));
            foreach ($paths as $path)
            {
                $tmp_pathname = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim($file, DIRECTORY_SEPARATOR);
                if (file_exists($tmp_pathname))
                {
                    $pathname = $tmp_pathname;
                    break;
                }
            }
        }
        return $pathname;
    }

    /**
     * Render a given template.
     * Merge data given in parameters with current data stored in the view.
     * View's data are not the same as the Twig's global data.
     * @param string $template The template's name.
     * @param array $data Any additonal data to be passed to the template for rendering.
     * @return string The rendered template's content.
     */
    protected function render($template, $data = null)
    {
        if (!empty($data))
        {
          if (!is_array()) throw new \ErrorException("Data to give to the view to render must be in an array !");
        }
        else
        {
          $data = array();
        }
        $data = array_merge($this->data->all(), $data);
        return $this->_renderer->render($template, $data);
    }

    /**
     * Return a reference to the current Twig renderer (environment).
     * @return Twig_Environment The current Twig renderer used by the view.
     */
    public function twig()
    {
        return $this->_renderer;
    }
}
