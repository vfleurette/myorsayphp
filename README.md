# Slim-Twig

This package contains a new view adapter to Twig template rendering engine for Slim based applications.

The new view object to use in your Slim app acts like a wrapper for the Twig environment object in charge of rendering a template.

# Requirements

This package is compatible with :

* Slim-Utils 1.3 or greater
* Slim 2.4 or greater
* Twig 1.6 o greater

# Installation

You have two options :

* Downloading from GitHub
  * You have only one class to include from `src` folder called `BenGee\Slim\Twig\TwigView`
  * Of course, you must previously import `Slim` and `Twig` bootstrap classes to your code before importing this one

* Downloading from Composer
  * Add the `bgillet/slim-twig` package to your `composer.json`
  * Then just include your `vendor/autoload.php` file as usual and its done

# Configuration

In your Slim configuration, set the `view` parameterc as follow :
> `'view' => new \BenGee\Slim\Twig\TwigView()`

By default, Slim manages only one template folder. You can set its default value through Slim configuration using :
> `'template.path' => '/.templates'`

The example above shows the Slim's default templates location.

With this new view adapter, you can now work with more than one templates folders. The `'template.path'` setting now accept either a single folder as string or multiple folders as an array of strings composed with a namespace as key and a folder path as value for each entry.

You can also set your templates folders programmatically using the `setTemplatesDirectory($directory)` method. In this case, the `$directory` parameter can be either a string or an array. There are many other methods that can be used to set, clear, add, remove, ..., templates directories. These methods will be described later.

# How to use

The `\BenGee\Slim\Twig\TwigView` class inherits `\Slim\View` class so all methods available in this parent class can be used as usual. The class can be registered and used like any standard Slim view adapter. See Slim Framework documentation for more details.

This view adapter uses a `\Twig_Loader_Filesystem` instance to locate templates on your filesystem. It also uses a `\Twig_Environment` instance to render templates.

You can access the Twig renderer from the view via the `twig()` accessor :
> $myView->twig()->enableDebug();

But you can also call Twig renderer methods straight from the view :
> $myView->enableDebug();

You can have more than one templates folders (see *`xxxTemplatesDirectory()`* method in the API documentation). Each template folder can have a namespace. Default template folder doesn't have one.

Let's imagine you have initialized your Twig environment with the following template folders :
> $myView->setTemplatesDirectory(array( null => './tpl', 'test' => '.tpl2' ));

You have a default templates folder located in `./tpl` and a second one located in `./tpl2` with a namespace named `test`.

If you call the `display()` method like below :
> $myView->display('myTemplate.twig', $myData);

Then the Twig renderer will look for your `myTemplate.twig` template file inside the `./tpl` default template folder.

If you want to call your template from the namespaced `test` template folder located in `./tpl2` then you must prefix your template name with `@` followed by your namespace then `/` like in the example below :
> $myView->display('@test/myTemplate.twig', $myData);

# API  

## Class `\BenGee\Slim\Twig\TwigView`

* `public function __construct()`
  * Default constructor. Create default `\Twig_Loader_Filesystem` and `\Twig_Environment' internal instances.
* `public function __call(string $name, array $arguments)`
  * PHP magic method used to redirect calls to methods not available in this class to the internal instance of the Twig renderer (`\Twig_Environment`). By this way all public methods of the Twig renderer are available straight from the view adapter.
* `public static function __callStatic(string $name, array $arguments)`
  * Same as `__call()` method but for static method calls.
* `public function setTemplatesDirectory($directory)`
  * Override parent `\Slim\View` method. Set one or more directories where Twig renderer will look for templates. Be aware that calling this method replaces currently set directories.
    * `$directory` : A single directory (string) or an array using namespace as key and path as value.
* `public function addTemplatesDirectory($path, $namespace = false)`
  * Add a templates directory to the **end** of the list of existing ones known by the current Twig templates loader.
    * `$path` : Path to the templates directory to add.
    * `$namespace` : Namespace used to reference the directory inside the Twig templates loader.
* `public function prependTemplatesDirectory($path, $namespace = false)`
  * Add a templates directory to the **beginning** of the list of existing ones known by the current Twig templates loader.
    * `$path` : Path to the templates directory to add.
    * `$namespace` : Namespace used to reference the directory inside the Twig templates loader.
* `public function getTemplatesDirectory()`
  * Get currently set templates directories.
    * Return an array of all referenced templates directories with their namespace as key and path as value.
* `public function getTemplatesNamespaces()`
  * Get templates directories namespaces.
    * Return an array of namespaces used by Twig loader to reference templates directories.
* `public function getTemplatePathname($file, $namespace = false)`
  * Get a fully qualified path to a given template.
    * `$file` : Filename of the template to find in referenced templates directories.
    * `$namespace` : Namespace of a specific set of templates directories.
    * Return the full pathname to the template as a string or 'false' if not found.
* `public function display($template, $data = null)`
  * Display (echo) content of a rendered template.
    * `$template` : Pathname of a template file relative to templates directory eventually identified by a namespace following the Twig syntax.
    * `$data` : Any additonal data to be passed to the template. These data will be merged with those already set in the view.
* `public function fetch($template, $data = null)`
  * Return content of a rendered template.
    * `$template` : Pathname of a template file relative to templates directory eventually identified by a namespace following the Twig syntax.
    * `$data` : Any additonal data to be passed to the template. These data will be merged with those already set in the view.
    * Return content of the rendered Template.
* `protected function render($template, $data = null)`
  * Render a template.
    * `$template` : Pathname of a template file relative to templates directory eventually identified by a namespace following the Twig syntax.
    * `$data` : Any additonal data to be passed to the template. These data will be merged with those already set in the view.
    * Return content of the rendered template.
* `public function twig()`
  * Return a reference to the current instance of Twig renderer used by the view.




