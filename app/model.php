<?php
function getTopparcours() {
	$sql = "Select Distinct `parcours`.nom,  `parcours`.date, `parcours`.duree , `parcours`.id_oeuvre ,`parcours`.type , `parcours`.note , `utilisateur`.nom , `utilisateur`.prenom from `parcours`  inner join `utilisateur`  ON `parcours`.`id_user` = `utilisateur`.id WHERE `utilisateur`.type != 'admin' AND `parcours`.type='public' order by `parcours`.note DESC";
	try {
		$db = getDB();
		$stmt = $db->query($sql);
		$top= $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $top;
}

function getRecentparcours($id) {
	$sql = "Select Distinct `parcours`.id as 'id', `parcours`.nom as 'nomparcours',  `parcours`.date, `parcours`.duree , `parcours`.id_oeuvre ,`parcours`.type , `parcours`.note , `utilisateur`.nom , `utilisateur`.prenom from `parcours`  inner join `utilisateur`  ON `parcours`.`id_user` = `utilisateur`.id WHERE `utilisateur`.id =:id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$historic= $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $historic;
}

function getArtistes(){
    $sql = "SELECT * FROM artiste ORDER BY id";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= json_encode($data);
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}

function getOeuvres(){
    $sql = "SELECT * FROM oeuvre ORDER BY id";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= json_encode($data);
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}
function getprocessplandata($id){
    $sql = "SELECT * FROM processparcours WHERE `processparcours`.id_user =:id";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= $data;
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}
function getplanbyid($id){
    $sql = "SELECT `processparcours`.parcours FROM processparcours WHERE `processparcours`.id =:id";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= $data;
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}


function getusersparcours(){
$sql = "select Distinct `parcours`.id ,`parcours`.nom AS nparcours ,  `parcours`.date, `parcours`.duree , `parcours`.id_oeuvre ,`parcours`.type , `parcours`.note , `utilisateur`.nom , `utilisateur`.prenom, `utilisateur`.mail from `parcours`  inner join `utilisateur`  ON `parcours`.`id_user` = `utilisateur`.id where `utilisateur`.type != 'admin' ";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result = '{"listuser": ' . json_encode($data) . '}';
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}

function getUser(){
    	$sql = "SELECT mail, nom, prenom, numero_telephone, date_naissance,type FROM utilisateur ORDER BY id";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= json_encode($data);
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}

function getData(){
    $sql="select DISTINCT
`oeuvre`.`id` as 'id_oeuvre',
`oeuvre`.`nom` as 'nom_oeuvre',
`oeuvre`.`description`as 'desc_oeuvre',
`oeuvre`.`date` as 'date_oeuvre',
`oeuvre`.`image`as 'href_image',
`oeuvre`.`periode`as 'periode',
`oeuvre`.`type` as 'type_oeuvre',
`artiste`.`id` as 'id_artiste',
`artiste`.`nom` as 'nom_artiste',
`position`.`id_position`,
`position`.`proche_bar`,
`position`.`proche_jus`,
`position`.`proche_magasin`,
`position`.`proche_marie`,
`position`.`proche_mcdo`,
`position`.`proche_restaurant`,
`position`.`proche_toilette`,
`goodies`.`nom` as 'nom_goodies',
`goodies`.`id_goodies`as 'id_goodies',
`goodies`.`type` as 'type_goodies',
`goodies`.`auteur`as 'auteur_goodies',
`goodies`.`editeur`as 'editeur_goodies',
`goodies`.`prix`as 'prix',
`goodies`.`description`as 'desc_goodies'
from oeuvre INNER JOIN `artiste` on `oeuvre`.`id_artiste`= `artiste`.`id` INNER JOIN `oeuvre_goodies` ON `oeuvre`.`id_goodies`= `oeuvre_goodies`.`goodies_id` INNER JOIN `goodies` on `goodies`.`id_goodies` = `oeuvre_goodies`.`goodies_id` INNER JOIN `position` on `position`.`id_position` = `oeuvre`.`id_position` order by `oeuvre`.`id`
";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result = '{"data": ' . json_encode($data) . '}';
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}
function gettypeoeuvre(){
    $sql="Select distinct type from `oeuvre`";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= $data;
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}

function gettypes(){
    $sql="Select distinct type from `oeuvre`";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= json_encode($data);
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}
function getmouvement(){
    $sql="Select distinct periode from `oeuvre`";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= json_encode($data);
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}

function saveparcours($data){

$nparcours =$data->{'nparcours'};
$date =$data->{'calendar'};
$hour =$data->{'hour'};
$public =$data->{'public'};
$arraydata =$data->{'arraydata'};
$longueur_array=count($arraydata)-1;
unset($arraydata[$longueur_array]);
$session=  $_SESSION['user'];
        $id=0;
        $sql = "INSERT INTO processparcours (nomparcours, date, duree, typeparcours, parcours, id_user,status) VALUES (:nomparcours, :date, :duree, :typeparcours, :parcours, :id_user,:status)";
    try {
        $db = getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("nomparcours", $nparcours);
        $stmt->bindParam("date", $date);
        $stmt->bindParam("duree", $hour);
        $stmt->bindParam("typeparcours", $public);
        $stmt->bindParam("parcours", json_encode($arraydata));
        $stmt->bindParam("id_user",$session['id'] );
        $stmt->bindParam("status",$id);
        $stmt->execute();
        $id = $db->lastInsertId();
        $db = null;
        $result= "Votre parcours a été enregistré avec succes !";
    } catch(PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
}
function removerowbyid($id){

     $sql ="DELETE FROM parcours WHERE id=:id;";
    	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		//$data = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$result= "Le parcours a été supprimé !";
    } catch(PDOException $e) {
        $result= '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return $result;
    }
?>
