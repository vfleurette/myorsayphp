<?php
session_start();
session_cache_limiter(false);
include 'db.php';
include 'app.php';
date_default_timezone_set('Europe/Paris');
require_once __DIR__ . '/../vendor/autoload.php';
$app = new \Slim\Slim(array(
    'view' => new \BenGee\Slim\Twig\TwigView(),
    'templates.path' => array(
        null => './layout',
        'test' => './views'
    )
));

$app->view->enableDebug();
$app->view->addExtension(new \Twig_Extension_Debug());
$app->view->addFilter(new \Twig_SimpleFilter('classname', 'get_class'));

$app->get('/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(!isset( $_SESSION['user'])){
    $app->render('@test/index.twig', array(
        'body' => 'home',
        'title' => 'Mon Musée d\'Orsay',
        'session'=>$session,
        'app' => $app
    ));
    }else{
    $app->redirect('/admin/');
    }
});

$app->get('/signin/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(!isset( $_SESSION['user'])){
    $app->render('@test/signin.twig', array(
        'body' => 'signin',
        'title' => 'S\'enregistrer',
        'session'=> $session,
        'app' => $app
    ));
    }else{
    $app->redirect('/admin/');
    }
});

$app->get('/login/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(!isset( $_SESSION['user'])){
    $app->render('@test/login.twig', array(
        'body' => 'login',
        'title' => 'Se connecter',
        'session'=> $session,
        'app' => $app
    ));
    }else{
    $app->redirect('/admin/');
    }
});

$app->get('/help/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
    $app->render('@test/help.twig', array(
        'body' => 'help',
        'title' => 'FAQ',
        'session'=> $session,
        'app' => $app
    ));
});

$app->get('/top/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
$top =getTopparcours();
    $app->render('@test/top.twig', array(
        'body' => 'top',
        'title' => 'TOP Parcours',
        'session'=> $session,
        'top' => $top,
        'app' => $app
    ));
});

$app->get('/about/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
    $app->render('@test/about.twig', array(
        'body' => 'about',
        'title' => 'A propos',
        'session'=> $session,
        'app' => $app
    ));
});

$app->get('/historic/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(isset( $_SESSION['user'])){
$processplan=getprocessplandata($session['id']);
$historic =getRecentparcours($session['id']);
    $app->render('@test/historic.twig', array(
        'body' => 'top',
        'title' => 'Mes parcours',
        'session'=> $session,
        'processplan'=> $processplan,
        'historic' => $historic,
        'app' => $app
    ));
    }else{
    $app->redirect('/admin/');
    }
});

$app->get('/createmap/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(isset( $_SESSION['user'])){
    $app->render('@test/newplan.twig', array(
        'body' => 'create',
        'title' => 'Cree mon parcours',
        'session'=> $session,
        'app' => $app
    ));
   }else{
$app->redirect('/');
}
});

$app->post('/firsteplan/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}

$firsteplan= $app->request()->post();
    $nparcours= $firsteplan['nparcours'];
    $calendar= $firsteplan['calendar'];
    $hour= $firsteplan['shour'];
    $public= $firsteplan['spublic'];
    $createparcours=[
                "nparcours" => $nparcours,
                "calendar" => $calendar,
                "hour" => $hour,
                "spublic" => $public
            ];
            $_SESSION['parcours']= $createparcours;
            $sessionparc=$_SESSION['parcours'];
if(isset( $_SESSION['user'])){
    $app->redirect('/selectmap/');
   }else{
$app->redirect('/');
}
});

$app->get('/selectmap/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(isset( $_SESSION['user'])){
//$historic =getRecentparcours($session['id']);
$typeoeuvre=gettypeoeuvre();
    $app->render('@test/selectoption.twig', array(
        'body' => 'create',
        'title' => 'Cree mon parcours',
        'session'=> $session,
        'create' => 'historic',
        'dataparcours' => $_SESSION['parcours'],
        'typeoeuvre' => $typeoeuvre,
        'app' => $app
    ));
       }else{
$app->redirect('/');
}
});
$app->post('/verifyparcours/', function () use ($app) {
    $data = trim($_POST["sdata"]);
    $obj = json_decode ($data);
    $status=saveparcours($obj);
    echo ($status);
});

$app->get('/admin/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(isset( $_SESSION['user'])){
    if($session['type'] == 'user'){
    $app->render('@test/admin.twig', array(
        'body' => 'admin',
        'title' => 'Admin',
        'session'=> $session,
        'nom'=> $session['nom'],
        'prenom'=> $session['prenom'],
        'app' => $app

    ));
    } else if ( $session['type'] == 'guide') {
    $app->render('@test/guide.twig', array(
        'body' => 'admin',
        'title' => 'Guide',
        'session'=> $session,
        'app' => $app

    ));
    }else if ( $session['type'] == 'admin') {
    $app->render('@test/moderate.twig', array(
        'body' => 'admin',
        'title' => 'Admin',
        'session'=> $session,
        'app' => $app

    ));
    }
}else{
$app->redirect('/');
}
});

$app->get('/profil/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(isset( $_SESSION['user'])){
    $app->render('@test/profil.twig', array(
        'body' => 'profil',
        'title' => 'Votre Profil',
        'session'=> $session,
        'app' => $app

    ));
}else{
$app->redirect('/');
}
});

$app->get('/editprofil/', function () use ($app) {
if(isset( $_SESSION['user'])){
  $session=  $_SESSION['user'];
}else{
$session=null;
}
if(isset( $_SESSION['user'])){
    $app->render('@test/editprofil.twig', array(
        'body' => 'editprofil',
        'title' => 'Modifier votre profil',
        'session'=> $session,
        'app' => $app

    ));
}else{
$app->redirect('/');
}
});


$app->get('/listuser/', function () use ($app) {
	$sql = "select * FROM utilisateur ORDER BY id";
	try {
		$db = getDB();
		$stmt = $db->query($sql);
		$listuser = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"listuser": ' . json_encode($listuser) . '}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
});




$app->post('/newuser/', function () use ($app) {
if(!isset( $_SESSION['user'])){
    $user= $app->request()->post();
    $email= $user['email'];
    $password1 = sha1($user['password1']);
    $password2 = sha1($user['password2']);
    $lastname = $user['lastname'];
    $firstname = $user['firstname'];
    $calendar = $user['calendar'];
    $phone = $user['phone'];
        $token=randomPassword();
        // test de l'adresse e-mail
    $atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';   // caractères autorisés avant l'arobase
    $domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)'; // caractères autorisés après l'arobase (nom de domaine)

    $regex = '/^' . $atom . '+' .   // Une ou plusieurs fois les caractères autorisés avant l'arobase
    '(\.' . $atom . '+)*' .         // Suivis par zéro point ou plus
                                    // séparés par des caractères autorisés avant l'arobase
    '@' .                           // Suivis d'un arobase
    '(' . $domain . '{1,63}\.)+' .  // Suivis par 1 à 63 caractères autorisés pour le nom de domaine
                                    // séparés par des points
    $domain . '{2,63}$/i';          // Suivi de 2 à 63 caractères autorisés pour le nom de domaine
        if(isset($email) || isset($password1) || isset($password2)){
            $sql = "select mail FROM utilisateur where mail=:mail";
            try {
                $db = getDB();
                $stmt = $db->prepare($sql);
                $stmt->bindParam("mail", $email);
                $stmt->execute();
                $listuser = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;
            } catch(PDOException $e) {
                echo '{"error":{"text":'. $e->getMessage() .'}}';
            }
            $error=0;
            if (!empty($listuser)) {
                $error= 1;
            }elseif (!preg_match($regex, $email)) {
                $error= 2;
            }elseif ( $password1 != $password2 ) {
                $error= 3;
            }elseif ( strlen($user['password1']) < 7 ) {
                $error= 4;
            }

            if ($error > 0) {
                if ( $error == 1 ) {
                    $app->flash('error', 'email deja existant !');
                    $app->redirect('/signin/');
                     //echo "email deja existant ! ";
                } elseif ($error == 2) {
                    $app->flash('error', 'email mal formatée !');
                    $app->redirect('/signin/');
                } elseif ($error == 3) {
                    $app->flash('error', 'les mots de passes sont différents !');
                    $app->redirect('/signin/');
                }elseif ($error == 4) {
                    $app->flash('error', 'Mots de passe trop court, il doit faire plus de sept caractères !');
                    $app->redirect('/signin/');
                }
            } else {
                $id=0;
                $ip=getRealIp();
                $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
                $sql = "INSERT INTO utilisateur (mail, password, nom, prenom, numero_telephone, date_naissance,city,region,pays,codepostal,type,token) VALUES (:mail, :password, :nom, :prenom, :numero_telephone, :date_naissance,:city,:region,:pays, :codepostal, :type, :token)";
                $type="user";
            try {
                $db = getDB();
                $stmt = $db->prepare($sql);
                $stmt->bindParam("mail", $email);
                $stmt->bindParam("password", $password1);
                $stmt->bindParam("nom", $lastname);
                $stmt->bindParam("prenom", $firstname);
                $stmt->bindParam("numero_telephone", $phone);
                $stmt->bindParam("date_naissance", $calendar);
                $stmt->bindParam("city", $details->city);
                $stmt->bindParam("region", $details->region);
                $stmt->bindParam("pays", $details->country);
                $stmt->bindParam("codepostal", $details->postal);
                $stmt->bindParam("type", $type);
                $stmt->bindParam("token", $token);
                $stmt->execute();
                $id = $db->lastInsertId();
                $db = null;
                //echo json_encode($wine);
            } catch(PDOException $e) {
                error_log($e->getMessage(), 3, '/var/tmp/php.log');
                echo '{"error":{"text":'. $e->getMessage() .'}}';
            }
                        $session=[
                        "id" => $id,
                        "mail" => $email,
                        "nom" => $lastname,
                        "prenom" => $firstname,
                        "numero_telephone" => $phone,
                        "date_naissance" => $calendar,
                        "city" => $details->city,
                        "region" => $details->region,
                        "pays" => $details->country,
                        "codepostal" => $details->postal,
                        "type" => $type,
                        "token" => $token
                    ];
                    $_SESSION['user']= $session;
                // Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: Orsay iServ.fr <server@iserv.fr>' . "\r\n";
                mail($email, "Bienvenue sur Mon Musée d'Orsay", generateEmail($lastname, $firstname, $email,$token), $headers);
                $app->flash('hello', 'Bienvenue, votre inscription a bien été enregistré vous allez recevoir un email de confirmation !');
                $app->redirect('/admin/');
            }
        }else{
        $app->flash('error', 'Il y une erreur l\email ou le mot de passe est vide');
        $app->redirect('/signin/');
        }
    }else{
    $app->redirect('/admin/');
    }
});


$app->post('/newlogin/', function() use ($app){
if(!isset( $_SESSION['user'])){
    $login= $app->request()->post();
    $email= $login['email'];
    $password = sha1($login['password']);
    if(isset($email) && isset($password)){
        $sql = "select * FROM utilisateur where mail=:mail AND password=:password";
        try {
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("mail", $email);
            $stmt->bindParam("password", $password);
            $stmt->execute();
            $varlogin = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
        if(!empty($varlogin)){
            $value=$varlogin[0];
            $session=[
                "id" => $value->id,
                "mail" => $value->mail,
                "nom" => $value->nom,
                "prenom" => $value->prenom,
                "numero_telephone" => $value->numero_telephone,
                "date_naissance" => $value->date_naissance,
                "city" => $value->city,
                "region" => $value->region,
                "pays" => $value->pays,
                "codepostal" => $value->codepostal,
                "type" => $value->type,
                "token" => $value->token
            ];
            $_SESSION['user']= $session;
            $app->redirect('/admin/');
        }else{
            $app->flash('error', 'Problème de connexion (email ou mot de passe invalides)');
            $app->redirect('/login/');
            //echo "error sql login";
        }
    }else{
        $app->flash('error', 'Les champs ne sont pas remplis correctement');
        $app->redirect('/login/');
        //echo "error les champs ne sont pas remplis correctement";
    }
} else{
$app->redirect('/admin/');
}
});
$app->get('/logout/', function () use ($app) {
session_destroy();
$app->redirect('/');
});

$app->post('/removebyid/', function () use ($app) {
 $id = trim($_POST["deleteid"]);
    $status=removerowbyid($id);
    echo($status);
});

$app->post('/showplanbyid/', function () use ($app) {
    $id = trim($_POST["planid"]);
    $status = getplanbyid($id);
    echo($status['0']->parcours);
});

$app->get('/error/', function () use ($app) {
$app->render('@test/error.twig', array(
        'body' => 'error',
        'title' => 'error votre profil',
        'app' => $app

    ));
});

$app->post('/api/getUser/', function () use ($app) {
    $data=getusersparcours();
	echo $data;
});


$app->post('/api/getArtistes/', function () use ($app) {
	$artistes=getArtistes();
	echo($artistes);
});

$app->post('/api/getType/', function () use ($app) {
	$type=gettypes();
	echo($type);
});

$app->post('/api/getOeuvres/', function () use ($app) {
	$oeuvres=getOeuvres();
	echo($oeuvres);
});

$app->post('/api/user/', function () use ($app) {

    $data=getUser();
	echo($data);
});

$app->post('/api/getGoodies/', function () use ($app) {

    $data=getGoodies();
	echo($data);
});

$app->post('/api/getData/', function () use ($app) {
    $data=getData();
	echo($data);
});

$app->post('/api/getmouvement/', function () use ($app) {
    $data=getmouvement();
	echo($data);
});
$app->run();